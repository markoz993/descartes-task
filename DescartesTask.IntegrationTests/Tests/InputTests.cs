﻿using DescartesTask.Sdk;
using System.Threading.Tasks;
using Xunit;

namespace DescartesTask.IntegrationTests.Tests
{
    public class InputTests
    {
        private readonly ApiClient _defaultClient;

        public InputTests(ApiClient defaultClient)
        {
            _defaultClient = defaultClient;
        }

        public async Task TestPutLeft()
        {
            var result = await _defaultClient
                .Inputs.PutLeft(1, "AAAAAA==")
                .ResultAsync();

            result.GetData();
            Assert.True(result.IsSuccessStatusCode);
        }

        public async Task TestPutRight()
        {
            var result = await _defaultClient
                .Inputs.PutLeft(1, "AAAAAA==")
                .ResultAsync();

            result.GetData();
            Assert.True(result.IsSuccessStatusCode);
        }

        public async Task TestDiff()
        {
            try
            {
                var result = await _defaultClient
                    .Inputs.GetDiff(1)
                    .ResultAsync();

                result.GetData();
                Assert.True(result.IsSuccessStatusCode);
            }
            finally
            {
                TestCleanup();
            }
        }

        private void TestCleanup()
        {
            // This method should clean up any possible leftovers of the test
        }
    }
}
