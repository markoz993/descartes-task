﻿using DescartesTask.Repository.Repositories.Entities;
using DescartesTask.RepositoriesAbstract.Entities;
using DescartesTask.RepositoriesAbstract;
using DescartesTask.Domain;
using System.Threading.Tasks;

namespace DescartesTask.Repository.Repositories
{
    public class RepositoryManager : IRepositoryManager
    {
        private RepositoryContext _repositoryContext;

        private IInputRepository _inputRepository;

        public RepositoryManager(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public IInputRepository Inputs
        {
            get
            {
                if (_inputRepository == null)
                    _inputRepository = new InputRepository(_repositoryContext);

                return _inputRepository;
            }
        }

        public Task SaveAsync() => _repositoryContext.SaveChangesAsync();
    }
}
