﻿using DescartesTask.Domain.Entities.Enums;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DescartesTask.Domain.Configuration.Enum
{
    public class InputTypeConfiguration : IEntityTypeConfiguration<InputType>
    {
        public void Configure(EntityTypeBuilder<InputType> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).HasMaxLength(256);
        }
    }
}