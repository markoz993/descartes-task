﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using DescartesTask.Domain.Entities;

namespace DescartesTask.Domain.Configuration
{
    public class InputConfiguration : IEntityTypeConfiguration<Input>
    {
        public void Configure(EntityTypeBuilder<Input> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Data).HasMaxLength(10240).IsRequired();
        }
    }
}