﻿using DescartesTask.Domain.Entities.Enums;
using DescartesTask.Domain.Extensions;
using DescartesTask.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DescartesTask.Domain
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
           : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyModelConfigurations();
            modelBuilder.ApplyEnumConfiguration();
        }

        public DbSet<Input> Inputs { get; set; }
        public DbSet<InputType> Types { get; set; }
    }
}