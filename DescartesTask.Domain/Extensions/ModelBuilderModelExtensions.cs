﻿using Microsoft.EntityFrameworkCore;
using DescartesTask.Domain.Configuration;

namespace DescartesTask.Domain.Extensions
{
    public static class ModelBuilderModelExtensions
    {
        public static void ApplyModelConfigurations(this ModelBuilder modelBuilder)
        {
            //Main models
            modelBuilder.ApplyConfiguration(new InputConfiguration());
        }
    }
}
