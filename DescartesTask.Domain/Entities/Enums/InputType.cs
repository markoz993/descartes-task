﻿using System.ComponentModel;

namespace DescartesTask.Domain.Entities.Enums
{
    public class InputType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
    }

    public enum InputTypeEnum
    {
        [Description("Not-Available")]
        NotAvailable = 0,

        [Description("Left")]
        Left = 1,

        [Description("Right")]
        Right = 2
    }
}
