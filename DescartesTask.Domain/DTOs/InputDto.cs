﻿using DescartesTask.Domain.Entities.Enums;
using System;

namespace DescartesTask.Domain.DTOs
{
    public class InputDto
    {
        public Guid Id { get; set; }

        public int OrderingId { get; set; }

        public DateTime TimeCreated { get; set; }

        public string Data { get; set; }

        public InputTypeEnum Type { get; set; }
    }
}
