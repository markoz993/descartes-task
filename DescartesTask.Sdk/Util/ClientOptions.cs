﻿using static DescartesTask.Sdk.ApiClient;
using DescartesTask.Sdk.Decompression;
using DescartesTask.Sdk.Formating;
using DescartesTask.Sdk.Retry;
using System;

namespace DescartesTask.Sdk.Util
{
    public class ClientOptions
    {
        public IHttpFormatter Formatter { get; set; }
        public IDecompressor Decompressor { get; set; }
        public IRetryPolicy RetryPolicy { get; set; }
        public ICompressor Compressor { get; set; }
        public TimeSpan? DefaultRequestTimeout { get; set; }
        public TokenCallback GetToken { get; set; }
    }
}
