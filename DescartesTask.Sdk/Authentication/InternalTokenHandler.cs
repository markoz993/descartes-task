﻿using DescartesTask.Sdk.Contracts;
using DescartesTask.Sdk.Formating;
using DescartesTask.Sdk.Util;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text;
using System.Net;
using System;

namespace DescartesTask.Sdk.Authentication
{
    internal class InternalTokenHandler
    {
        private readonly IHttpFormatter _formatter;
        private readonly HttpClient _httpClient;

        public InternalTokenHandler(HttpClient httpClient)
        {
            var formatterSettings = new JsonSerializerSettings { ContractResolver = new SnakeCaseContractResolver() };
            _formatter = new JsonHttpFormatter(formatterSettings);
            _httpClient = httpClient;
        }

        internal virtual async Task<ApiResult<TokenResponseDto>> GetClientAccessTokenAsync(string clientId, string clientSecret, IEnumerable<string> scopes)
        {
            var uri = "internal/connect/token";
            var request = SetupAuthenticationRequest(uri, clientId, clientSecret, scopes);
            HttpResponseMessage response;

            try
            {
                response = await _httpClient.SendAsync(request);
            }
            catch (HttpRequestException ex)
            {
                var error = new ErrorDto
                {
                    Message = ex.Message,
                    ErrorCode = ErrorCodes.AuthorityEndpointFailiure,
                    IsRetryable = true
                };

                return new ApiResult<TokenResponseDto>(HttpStatusCode.InternalServerError, error);
            }

            if (response.StatusCode == HttpStatusCode.ServiceUnavailable)
                return new ApiResult<TokenResponseDto>(HttpStatusCode.ServiceUnavailable, new ErrorDto
                {
                    Message = "Service Unavailable: " + response.RequestMessage.RequestUri,
                    ErrorCode = ErrorCodes.AuthorityServiceUnavailable,
                    IsRetryable = true
                });

            using (var stream = await response.Content.ReadAsStreamAsync())
            {
                TokenResponseDto tokenResponse;
                try
                {
                    tokenResponse = _formatter.DeserializeObject<TokenResponseDto>(stream);
                    tokenResponse.Expires = DateTimeOffset.UtcNow.AddSeconds(tokenResponse.ExpiresIn);

                    if (!string.IsNullOrEmpty(tokenResponse.Error))
                        return new ApiResult<TokenResponseDto>(response.StatusCode, new ErrorDto
                        {
                            Message = tokenResponse.Error,
                            ErrorCode = ErrorCodes.AuthorityUnableToGetToken,
                            IsRetryable = false
                        });
                }
                catch (Exception ex)
                {
                    return new ApiResult<TokenResponseDto>(response.StatusCode, new ErrorDto
                    {
                        Message = ex.Message,
                        ErrorCode = ErrorCodes.AuthorityUnableToFormatToken,
                        StackTrace = ex.StackTrace,
                        IsRetryable = false
                    });
                }

                return new ApiResult<TokenResponseDto>(response.StatusCode, tokenResponse);
            }
        }

        private static HttpRequestMessage SetupAuthenticationRequest(string uri, string clientId, string clientSecret, IEnumerable<string> scopes)
        {
            var authHeaderBytes = Encoding.UTF8.GetBytes($"{clientId}:{clientSecret}");
            var basicAuthentication = Convert.ToBase64String(authHeaderBytes);
            var joinedScopes = string.Join(" ", scopes);

            var content = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("grant_type", "client_credentials"),
                new KeyValuePair<string, string>("scope", joinedScopes)
            };

            var request = new HttpRequestMessage(HttpMethod.Post, uri)
            {
                Content = new FormUrlEncodedContent(content)
            };

            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", basicAuthentication);

            return request;
        }
    }
}
