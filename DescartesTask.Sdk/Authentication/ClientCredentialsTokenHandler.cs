﻿using System.Threading.Tasks;
using System.Net.Http;
using System;

namespace DescartesTask.Sdk.Authentication
{
    public class ClientCredentialsTokenHandler : ITokenHandler, IBearerTokenHandler
    {
        private readonly string _clientId;
        private readonly string _clientSecret;
        private readonly InternalTokenHandler _internalTokenHandler;
        private readonly ClientCredentialsOptions _options;

        public ClientCredentialsTokenHandler(HttpClient httpClientSignIn, ClientCredentialsOptions options)
        {
            _clientId = options.ClientId;
            _clientSecret = options.ClientSecret;
            _internalTokenHandler = new InternalTokenHandler(httpClientSignIn);
            _options = options;
        }

        public async Task<ApiResult<string>> GetBearerTokenAsync()
        {
            var token = await GetTokenAsync();
            return token.ToBearerTokenResult();
        }

        public async Task<ApiResult<TokenResponseDto>> GetTokenAsync()
        {
            ApiResult<TokenResponseDto> token = GetCachedToken(DateTimeOffset.UtcNow);
            if (token != null)
                return token;

            token = await FetchTokenAsync();
            if (token.IsSuccessStatusCode)
                SetCachedToken(token);

            return token;
        }

        protected async Task<ApiResult<TokenResponseDto>> FetchTokenAsync() =>
            await _internalTokenHandler.GetClientAccessTokenAsync(_clientId, _clientSecret, new[] { "DescartesTask" });

        internal ApiResult<TokenResponseDto> GetCachedToken(DateTimeOffset dateTime)
        {
            if (_options.TokenCache == null)
                return null;

            const int expireBuffer = 30;
            if (dateTime.AddSeconds(expireBuffer) < _options.TokenCache.Item1)
                return _options.TokenCache.Item2;

            return null;
        }

        private void SetCachedToken(ApiResult<TokenResponseDto> tokenResponse)
        {
            if (tokenResponse.Data == null)
                return;
            
            tokenResponse.Data.Expires = tokenResponse.Data.Expires ?? DateTimeOffset.UtcNow.AddSeconds(tokenResponse.Data.ExpiresIn);
            _options.TokenCache = new Tuple<DateTimeOffset, ApiResult<TokenResponseDto>>(tokenResponse.Data.Expires.Value, tokenResponse);
        }
    }
}
