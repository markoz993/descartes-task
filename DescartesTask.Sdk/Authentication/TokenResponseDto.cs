﻿using System;

namespace DescartesTask.Sdk.Authentication
{
    public class TokenResponseDto
    {
        public string AccessToken { get; set; }

        public string IdentityToken { get; set; }

        public string Error { get; set; }

        /// <summary>
        /// Time when token will expire
        /// </summary>
        public DateTimeOffset? Expires { get; set; }

        /// <summary>
        /// Time in seconds until token expires
        /// </summary>
        public long ExpiresIn { get; set; }

        public string TokenType { get; set; }

        public string RefreshToken { get; set; }
    }
}
