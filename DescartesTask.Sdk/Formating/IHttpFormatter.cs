﻿using System.IO;

namespace DescartesTask.Sdk.Formating
{
    public interface IHttpFormatter
    {
        T DeserializeObject<T>(Stream responseStream);

        string SerializeObject(object obj);
    }
}
