﻿using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Text;

namespace DescartesTask.Sdk.Formating
{
    public class SnakeCaseContractResolver : DefaultContractResolver
    {
        private const string Separator = "_";

        protected override string ResolvePropertyName(string propertyName)
        {
            var parts = new List<string>();
            var currentWord = new StringBuilder();

            foreach (var c in propertyName)
            {
                if (char.IsUpper(c) && currentWord.Length > 0)
                {
                    parts.Add(currentWord.ToString());
                    currentWord.Clear();
                }
                currentWord.Append(char.ToLower(c));
            }

            if (currentWord.Length > 0)
                parts.Add(currentWord.ToString());

            return string.Join(Separator, parts.ToArray());
        }
    }
}
