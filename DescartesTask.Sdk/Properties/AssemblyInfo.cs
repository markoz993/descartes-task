﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DescartesTask.UnitTests")]
[assembly: InternalsVisibleTo("DescartesTask.InMemoryTests")]