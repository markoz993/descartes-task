﻿using System.Globalization;
using System.Text;

namespace DescartesTask.Sdk.Contracts
{
    public class ErrorDto
    {
        /// <summary>
        /// The resource (i.e. endpoint) where the error occured.
        /// </summary>
        public string Resource { get; set; }

        public string ErrorCode { get; set; }

        public bool IsRetryable { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine().AppendFormat(CultureInfo.InvariantCulture, "Error code: {0}", ErrorCode).AppendLine();
            stringBuilder.AppendFormat(CultureInfo.InvariantCulture, "Error message: {0}", Message).AppendLine();

            return stringBuilder.ToString();
        }
    }
}
