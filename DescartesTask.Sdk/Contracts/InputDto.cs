﻿namespace DescartesTask.Sdk.Contracts
{
    public class InputDto
    {
        public string Data { get; set; }
    }
}
