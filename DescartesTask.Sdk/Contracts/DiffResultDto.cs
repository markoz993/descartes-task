﻿using System.Collections.Generic;

namespace DescartesTask.Sdk.Contracts
{
    public class DiffResultDto
    {
        public InputDto Left { get; set; }
        public InputDto Right { get; set; }

        public string Message { get; set; }
        public string DiffResultType { get; set; }

        public IEnumerable<DiffDto> Diffs { get; set; }
    }
}
