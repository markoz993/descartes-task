﻿using System.Threading.Tasks;
using System;

namespace DescartesTask.Sdk.Retry
{
    public interface IRetryPolicy
    {
        Task<ApiResult<T>> RunAsync<T>(Func<Task<ApiResult<T>>> func);
    }
}
