﻿using System.Threading.Tasks;
using System;

namespace DescartesTask.Sdk.Retry
{
    public class NoRetryPolicy : IRetryPolicy
    {
        public async Task<ApiResult<T>> RunAsync<T>(Func<Task<ApiResult<T>>> func) =>
            await func();
    }
}
