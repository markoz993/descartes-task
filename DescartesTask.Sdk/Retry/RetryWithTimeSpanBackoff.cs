﻿using System.Threading.Tasks;
using System;

namespace DescartesTask.Sdk.Retry
{
    public class RetryWithTimeSpanBackoff : IRetryPolicy
    {
        private readonly TimeSpan[] _retryDelays;

        public RetryWithTimeSpanBackoff()
        {
            _retryDelays = new[] {
                TimeSpan.FromMilliseconds(500),
                TimeSpan.FromSeconds(2),
                TimeSpan.FromSeconds(5)
            };
        }

        public RetryWithTimeSpanBackoff(TimeSpan[] retries)
        {
            _retryDelays = retries;
        }

        public async Task<ApiResult<T>> RunAsync<T>(Func<Task<ApiResult<T>>> func) =>
            await MakeCall(0, func);

        private async Task<ApiResult<T>> MakeCall<T>(int retryCount, Func<Task<ApiResult<T>>> func)
        {
            var result = await func();

            /*429 = TooManyRequests*/
            var statusCodeInt = (int)result.StatusCode;
            bool transient =
                statusCodeInt == 429 ||
                statusCodeInt >= 500 && statusCodeInt < 600 ||
                result.StatusCode == System.Net.HttpStatusCode.RequestTimeout;

            ++retryCount;

            bool shouldRetry = !result.IsSuccessStatusCode && result.Error?.IsRetryable == true && transient;
            if (shouldRetry && retryCount <= _retryDelays.Length)
            {
                //Wait until next retry
                var delay = _retryDelays[retryCount - 1];
                await Task.Delay(delay);

                //Retry
                return await MakeCall(retryCount, func);
            }

            //Return response
            return result;
        }
    }
}