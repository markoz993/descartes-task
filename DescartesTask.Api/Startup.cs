using DescartesTask.Service.AutoMapper;
using DescartesTask.Api.Extensions;
using DescartesTask.Api.Middleware;
using DescartesTask.Sdk.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO.Compression;
using System.Linq;
using System.Net;
using AspNetCoreRateLimit;
using Newtonsoft.Json;

namespace DescartesTask.Api
{
    public class Startup
    {
        public IWebHostEnvironment HostingEnvironment { get; set; }
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            HostingEnvironment = env;
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var mapper = AutoMapperConfiguration.Configure();
            services.AddSingleton(mapper);

            //Cookies
            services.Configure<CookiePolicyOptions>(options => { options.MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.None; });

            services.ConfigureCors();
            services.ConfigureServerIntegration();
            services.ConfigureSqlContext(Configuration);
            services.ConfigureRepositoryManager();

            services.AddRepositories();
            services.AddServices();
            services.AddOptions(Configuration);
            services.AddHelpers(Configuration);

            services.ConfigureResponseCaching();
            services.ConfigureHttpCacheHeaders();

            services.AddHttpContextAccessor();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(jwt => {
                    jwt.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = async context => {
                            //Return error message when authentication fail
                            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            var headers = context.Response.GetTypedHeaders();
                            headers.ContentType = new MediaTypeHeaderValue("text/json");
                            await context.Response.WriteAsync(JsonConvert.SerializeObject(new ErrorDto
                            {
                                ErrorCode = "AUTHENTICATION_FAILED",
                                Message = context.Exception.Message,
                                StackTrace = context.Exception.StackTrace
                            }));
                        }
                    };

                    jwt.Authority = Configuration["Authority"] + "internal/";
                    jwt.Audience = Configuration["Authority"] + "internal/resources";
                });

            services.AddMemoryCache();
            services.AddHttpContextAccessor();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();

            services.AddAuthorization(options => {
                options.DefaultPolicy = new AuthorizationPolicyBuilder()
                  .RequireAuthenticatedUser()
                  .Build();
            });

            services.Configure<GzipCompressionProviderOptions>(options => {
                options.Level = CompressionLevel.Fastest;
            });

            services.AddResponseCompression(options => {
                options.Providers.Add<BrotliCompressionProvider>();
                options.Providers.Add<GzipCompressionProvider>();
                options.EnableForHttps = true;

                //Add extra types to compress besides the default ones
                options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                    new[] {
                        "image/svg+xml"
                    }
                );
            });

            services.AddControllers(config =>
            {
                config.RespectBrowserAcceptHeader = true;
                config.ReturnHttpNotAcceptable = true;
                config.CacheProfiles.Add("120SecondsDuration", new CacheProfile { Duration = 120 });
            }).AddNewtonsoftJson();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.EnvironmentName == "development")
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<DecompressRequestMiddleware>();

            app.UseCookiePolicy();
            app.UseCors("CorsPolicy");
            app.UseForwardedHeaders(new ForwardedHeadersOptions { ForwardedHeaders = ForwardedHeaders.All });
            app.UseResponseCaching();
            app.UseResponseCompression();
            app.UseHttpCacheHeaders();

            app.Use(async (context, next) => {
                await next.Invoke();

                if (!context.Items.ContainsKey("StoreForLogging"))
                {
                    if (context.Response.StatusCode == StatusCodes.Status401Unauthorized)
                    {
                        await context.Response.WriteAsync(
                            JsonConvert.SerializeObject(new ErrorDto
                            {
                                ErrorCode = "UNAUTHORIZED",
                                Message = "Unauthorized",
                                StackTrace = string.Empty
                            }));
                    }

                    if (context.Response.StatusCode == StatusCodes.Status404NotFound)
                    {
                        await context.Response.WriteAsync(
                            JsonConvert.SerializeObject(new ErrorDto
                            {
                                Message = $"Endpoint \"{context.Request.Method} {context.Request.GetDisplayUrl()}\" not found. ",
                                ErrorCode = "EndpointNotFound"
                            }));
                    }
                }
            });

            app.Map("/api", apiContext => {
                apiContext.UseHttpsRedirection();
                apiContext
                    .UseRouting()
                    .UseAuthentication()
                    .UseAuthorization()
                    .UseEndpoints(endpoints => {
                        endpoints.MapControllers();
                    });
            });
        }
    }
}