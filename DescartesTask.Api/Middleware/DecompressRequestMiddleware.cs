﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO.Compression;
using System.IO;
using System;

namespace DescartesTask.Api.Middleware
{
    public class DecompressRequestMiddleware
    {
        private readonly RequestDelegate _next;
        private const string ContentEncodingHeader = "Content-Encoding";
        private const string ContentEncodingGzip = "gzip";
        private const string ContentEncodingDeflate = "deflate";
        private const string ContentEncodingBrotli = "br";

        public DecompressRequestMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.Request.Headers.Keys.Contains(ContentEncodingHeader))
            {
                await _next(context);
                return;
            }

            var contentEncoding = context.Request.Headers[ContentEncodingHeader];
            if (!new List<string> { ContentEncodingGzip, ContentEncodingDeflate, ContentEncodingBrotli }.Contains(contentEncoding))
            {
                await _next(context);
                return;
            }

            Stream decompressor;

            if (contentEncoding == ContentEncodingGzip)
                decompressor = new GZipStream(context.Request.Body, CompressionMode.Decompress, true);
            else if (contentEncoding == ContentEncodingDeflate)
                decompressor = new DeflateStream(context.Request.Body, CompressionMode.Decompress, true);
            else
                decompressor = new BrotliStream(context.Request.Body, CompressionMode.Decompress, true);

            context.Request.Body = decompressor;
            await _next(context);
        }
    }
}