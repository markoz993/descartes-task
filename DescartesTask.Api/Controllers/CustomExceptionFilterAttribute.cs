﻿using DescartesTask.Sdk.Contracts;
using DescartesTask.Sdk.Util;
using DescartesTask.Sdk;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System.Security.Authentication;
using System.Text;
using System.Net;

namespace DescartesTask.Api.Controllers
{
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger<CustomExceptionFilterAttribute> _logger;

        public CustomExceptionFilterAttribute(ILogger<CustomExceptionFilterAttribute> logger)
        {
            _logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            _logger.LogError(context.Exception, context.Exception.Message);

            var exception = context.Exception;
            var statusCode = HttpStatusCode.InternalServerError;

            //Default error
            ErrorDto error = new ErrorDto
            {
                ErrorCode = ErrorCodes.InternalServerError,
                Message = exception.Message,
                Resource = string.Empty,
                StackTrace = exception.StackTrace
            };

            if (exception is Microsoft.EntityFrameworkCore.DbUpdateException dbException)
            {
                var sb = new StringBuilder(exception.Message + " Details: " + exception.InnerException?.Message);

                foreach (var eve in dbException.Entries)
                {
                    sb.Append($" - Entity of type {eve.Entity.GetType().Name} in state {eve.State} could not be updated. OriginalValue {eve.OriginalValues}. New Value: {eve.CurrentValues}");
                }

                error.Message = sb.ToString();
                error.ErrorCode = ErrorCodes.DatabaseError;
                statusCode = HttpStatusCode.InternalServerError;
            }

            if (exception is AuthenticationException)
            {
                error.ErrorCode = ErrorCodes.NotAuthenticated;
                statusCode = HttpStatusCode.Unauthorized;
            }

            if (exception is ApiHttpException calendarApiHttpException)
            {
                error = calendarApiHttpException.Error;
                statusCode = calendarApiHttpException.StatusCode;
            }

            if (context.HttpContext != null)
            {
                context.HttpContext.Items["StoreForLogging"] = new ErrorDto
                {
                    Message = error.Message,
                    StackTrace = error.StackTrace,
                    ErrorCode = statusCode.ToString()
                };
            }

            context.Result = new ObjectResult(error)
            {
                StatusCode = (int)statusCode
            };
        }
    }
}
