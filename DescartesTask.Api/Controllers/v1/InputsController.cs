﻿using DescartesTask.Service.ServicesAbstract;
using DescartesTask.Sdk.Contracts;
using DescartesTask.Sdk.Util;
using DescartesTask.Sdk;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Net;

namespace DescartesTask.Api.Controllers.v1
{
    [Authorize]
    [Route("v1/diff")]
    [ApiController]
    public class InputsController : ControllerBase
    {
        private readonly IInputService _service;
        private readonly IValidationService _validator;

        public InputsController(IInputService service, IValidationService validator)
        {
            _service = service;
            _validator = validator;
        }

        [HttpPut("{id:int}/left")]
        public async Task<bool> PutLeft(int id, [FromBody] string data)
        {
            if (!_validator.ValidateId(id))
                throw new ApiHttpException(HttpStatusCode.BadRequest, ErrorCodes.BadRequest, "Id provided is not valid.");

            if (!_validator.ValidatePayload(data))
                throw new ApiHttpException(HttpStatusCode.BadRequest, ErrorCodes.BadRequest, "Request payload is not valid");

            return await _service.SaveLeft(id, data);
        }

        [HttpPut("{id:int}/right")]
        public async Task<bool> PutRight(int id, [FromBody] string data)
        {
            if (!_validator.ValidateId(id))
                throw new ApiHttpException(HttpStatusCode.BadRequest, ErrorCodes.BadRequest, "Id provided is not valid.");

            if (!_validator.ValidatePayload(data))
                throw new ApiHttpException(HttpStatusCode.BadRequest, ErrorCodes.BadRequest, "Request payload is not valid");

            return await _service.SaveRight(id, data);
        }

        [HttpGet("{id:int}")]
        public async Task<DiffResultDto> GetDiff(int id)
        {
            if (!_validator.ValidateId(id))
                throw new ApiHttpException(HttpStatusCode.BadRequest, ErrorCodes.BadRequest, "Id provided is not valid.");

            var result = await _service.GetDiff(id);
            if (result == null)
                throw new ApiHttpException(HttpStatusCode.NotFound, ErrorCodes.NotFound, $"Diff with id: {id} not found.");

            return result;
        }
    }
}