﻿using DescartesTask.Service.ServicesAbstract;
using DescartesTask.Domain.Entities.Enums;
using DescartesTask.RepositoriesAbstract;
using DescartesTask.Service.Helpers;
using DescartesTask.Domain.Entities;
using DescartesTask.Domain.Filters;
using DescartesTask.Sdk.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using AutoMapper;

namespace DescartesTask.Service.Services
{
    public class InputService : IInputService
    {
        private readonly IRepositoryManager _manager;
        private readonly IConversionHelpers _conversion;
        private readonly IMapper _mapper;

        public InputService(IRepositoryManager manager, IConversionHelpers conversion, IMapper mapper)
        {
            _manager = manager;
            _conversion = conversion;
            _mapper = mapper;
        }

        public async Task<bool> SaveLeft(int id, string data)
        {
            var existingInputs = await _manager.Inputs.Get(x => x.OrderingId == id, null, true);

            //If there are no results we need to create a new one. If there are results and none of them are for the left side we can then create a new one
            if (existingInputs == null && !existingInputs.Any() || existingInputs != null && !existingInputs.Any(x => x.TypeId == (int)InputTypeEnum.Left))
            {
                CreateNewInput(id, data, InputTypeEnum.Left);
            }
            else
            {
                //Foreach loop should never have to go over more than a single input, I added it to mimic a more complex scenario
                existingInputs.Where(x => x.TypeId == (int)InputTypeEnum.Left);
                foreach (var input in existingInputs)
                    input.Data = data;
            }

            try
            {
                await _manager.SaveAsync();
                return true;
            }
            catch (Exception)
            {
                //Create error or log the error
                return false;
            }
        }

        public async Task<bool> SaveRight(int id, string data)
        {
            var existingInputs = await _manager.Inputs.Get(x => x.OrderingId == id, null, true);

            //If there are no results we need to create a new one. If there are results and none of them are for the right side we can then create a new one
            if (existingInputs == null && !existingInputs.Any() || existingInputs != null && !existingInputs.Any(x => x.TypeId == (int)InputTypeEnum.Right))
            {
                CreateNewInput(id, data, InputTypeEnum.Right);
            }
            else
            {
                //Foreach loop should never have to go over more than a single input, I added it to mimic a more complex scenario
                existingInputs.Where(x => x.TypeId == (int)InputTypeEnum.Left);
                foreach (var input in existingInputs)
                    input.Data = data;
            }

            try
            {
                await _manager.SaveAsync();
                return true;
            }
            catch (Exception)
            {
                //Create error or log the error
                return false;
            }
        }

        public async Task<DiffResultDto> GetDiff(int id)
        {
            var leftInputs = await _manager.Inputs.GetWithFilters(new InputFilter { OrderId = id }, x => x.TypeId == (int)InputTypeEnum.Left);
            var rightInputs = await _manager.Inputs.GetWithFilters(new InputFilter { OrderId = id }, x => x.TypeId == (int)InputTypeEnum.Right);

            if ((leftInputs == null || !leftInputs.Any()) && (rightInputs == null || !rightInputs.Any()))
                return new DiffResultDto { Message = "No inputs were found." };

            if (!leftInputs.Any() || !rightInputs.Any())
                return new DiffResultDto { Message = "Missing inputs." };

            return CalculateTheResults(leftInputs.FirstOrDefault(), rightInputs.FirstOrDefault());
        }

        private void CreateNewInput(int id, string data, InputTypeEnum type)
        {
            var input = new Input
            {
                OrderingId = id,
                Data = data,
                TimeCreated = DateTime.Now,
                TypeId = (int)type
            };

            _manager.Inputs.Create(input);
        }

        private DiffResultDto CalculateTheResults(Input left, Input right)
        {
            var result = new DiffResultDto
            {
                Left = _mapper.Map<InputDto>(left),
                Right = _mapper.Map<InputDto>(right),
                DiffResultType = "Equals"
            };

            var left64 = _conversion.Base64Encode(left.Data);
            var right64 = _conversion.Base64Encode(right.Data);

            if (left64 == right64)
                return result;

            if(left64.Length != right64.Length)
            {
                result.DiffResultType = "Sizes do not match";

                return result;
            }

            result.DiffResultType = "Content does not match";
            result.Diffs = CalculateDiffs(left64, right64);

            return result;
        }

        private List<DiffDto> CalculateDiffs(string left, string right)
        {
            var result = new List<DiffDto>();

            var counter = 1;
            for(int i = 0; i < left.Length; i++)
            {
                if (left.Substring(i, 1) != right.Substring(i, 1))
                {
                    var j = i;
                    while(left.Substring(j, 1) != right.Substring(j, 1) && j < left.Length)
                    {
                        counter++;
                        j++;
                    }
                    result.Add(new DiffDto { Length = counter, Offset = i });
                    counter = 1;
                }
            }

            return result;
        }
    }
}
