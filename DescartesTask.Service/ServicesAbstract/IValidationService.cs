﻿namespace DescartesTask.Service.ServicesAbstract
{
    public interface IValidationService
    {
        bool ValidateId(int id);

        bool ValidatePayload(string data);
    }
}
