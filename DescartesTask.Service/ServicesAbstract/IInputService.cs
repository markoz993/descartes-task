﻿using DescartesTask.Sdk.Contracts;
using System.Threading.Tasks;

namespace DescartesTask.Service.ServicesAbstract
{
    public interface IInputService
    {
        Task<DiffResultDto> GetDiff(int id);

        Task<bool> SaveLeft(int id, string data);
        Task<bool> SaveRight(int id, string data);
    }
}
