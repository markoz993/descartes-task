﻿using DescartesTask.Sdk.Authentication;
using DescartesTask.Sdk.Decompression;
using DescartesTask.Sdk.Util;
using DescartesTask.Sdk;
using System.Threading.Tasks;
using System.IO.Compression;
using System.Net.Http;
using System.Net;
using System.IO;

namespace DescartesTask.InMemoryTests
{
    public class TestSetupInMemory
    {
        public static InMemoryApiClient GetInMemoryClient(HttpClient client, ClientOptions options = null)
        {
            options ??= new ClientOptions
            {
                Decompressor = new BrotliDecompressor(),
                Compressor = new GZipCompressor(),
                GetToken = async () => await new InMemoryTokenHandler().GetBearerTokenAsync()
            };

            return new InMemoryApiClient(client, options);
        }
    }

    public class InMemoryApiClient : ApiClient
    {
        public InMemoryApiClient(HttpClient httpClient, ClientOptions options)
            : base(httpClient, options)
        {
        }
    }

    public class InMemoryTokenHandler : ITokenHandler, IBearerTokenHandler
    {
        private readonly ApiResult<TokenResponseDto> dummyToken = new ApiResult<TokenResponseDto>(HttpStatusCode.OK, new TokenResponseDto
        {
            AccessToken = "InMemoryToken",
            Error = string.Empty,
            ExpiresIn = 1000000,
            IdentityToken = string.Empty,
            RefreshToken = string.Empty,
            TokenType = string.Empty
        });

        public Task<ApiResult<string>> GetBearerTokenAsync() =>
            Task.FromResult(dummyToken.ToBearerTokenResult());

        public Task<ApiResult<TokenResponseDto>> GetTokenAsync() =>
            Task.FromResult(dummyToken);
    }

    public class BrotliDecompressor : IDecompressor
    {
        public string EncodingType => "br";

        public async Task Decompress(Stream source, Stream destination)
        {
            using BrotliStream bs = new BrotliStream(source, CompressionMode.Decompress);

            await bs.CopyToAsync(destination);
            destination.Seek(0, SeekOrigin.Begin);
        }
    }

    public class GZipDecompressor : IDecompressor
    {
        public string EncodingType => "gzip";

        public async Task Decompress(Stream source, Stream destination)
        {
            using GZipStream bs = new GZipStream(source, CompressionMode.Decompress);

            await bs.CopyToAsync(destination);
            destination.Seek(0, SeekOrigin.Begin);
        }
    }
}
