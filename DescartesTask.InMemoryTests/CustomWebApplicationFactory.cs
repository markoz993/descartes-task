﻿using DescartesTask.Domain;
using Microsoft.Extensions.DependencyInjection;

namespace DescartesTask.InMemoryTests
{
    public class CustomWebApplicationFactoryWithAuthentication<Startup> : CustomWebApplicationFactory<Startup>
    {
    }

    public class CustomWebApplicationFactory<TStartup>
    {
        private static void MockDatabase(IServiceCollection services)
        {
            //For some reason I was unable to create in-memory database and use it later on in unit tests 
            //UseInMemoryDatabase does not work for me
            //It must be hapening because of some EF updates
        }

        private static void SeedDatabase(RepositoryContext appDb)
        {
            // Seed the database with some specific test data.
        }
    }
}
