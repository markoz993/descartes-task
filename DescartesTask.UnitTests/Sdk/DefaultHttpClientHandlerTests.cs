﻿using DescartesTask.Sdk.Util;
using System.Net;
using Xunit;

namespace DescartesTask.UnitTests.Sdk
{
    public class DefaultHttpClientHandlerTests
    {
        [Fact]
        public void TestHandlerShouldSupportGZipAndDeflate()
        {
            var handler = new DefaultHttpClientHandler();
            Assert.True((handler.AutomaticDecompression & DecompressionMethods.GZip) == DecompressionMethods.GZip);
            Assert.True((handler.AutomaticDecompression & DecompressionMethods.Deflate) == DecompressionMethods.Deflate);
        }
    }
}
