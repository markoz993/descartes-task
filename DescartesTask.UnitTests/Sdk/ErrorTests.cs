﻿using DescartesTask.Sdk.Contracts;
using Xunit;

namespace DescartesTask.UnitTests.Sdk
{
    public class ErrorTests
    {
        [Fact]
        public void TestCreateErrorString()
        {
            var error = new ErrorDto
            {
                ErrorCode = "code",
                Message = "description"
            };

            var errorString = error.ToString();

            Assert.Equal(GetExpectedErrorString(), errorString);
        }

        private string GetExpectedErrorString()
        {
            return "\r\nError code: code\r\nError message: description\r\n";
        }
    }
}
