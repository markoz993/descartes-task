﻿using DescartesTask.Sdk.Authentication;
using DescartesTask.Domain.Entities;
using DescartesTask.Sdk.Contracts;
using DescartesTask.Sdk;
using System.Threading.Tasks;
using System.Net;
using Xunit;

namespace DescartesTask.UnitTests.Sdk
{
    public class ApiResultTests
    {
        [Fact]
        public void TestResultConstructor()
        {
            var badRequest = ApiResult<Input>.BadRequest();
            Assert.Equal(HttpStatusCode.BadRequest, badRequest.StatusCode);

            var badRequestWithMessage = ApiResult<Input>.BadRequest("Bad request");
            Assert.Equal(HttpStatusCode.BadRequest, badRequestWithMessage.StatusCode);

            var notFound = ApiResult<Input>.NotFound();
            Assert.Equal(HttpStatusCode.NotFound, notFound.StatusCode);

            Assert.ThrowsAny<ApiHttpException>(() => {
                new ApiResult<Input>(HttpStatusCode.OK,
                    new ErrorDto
                    {
                        ErrorCode = "Code",
                        Message = "message"
                    }).GetData();
            });
        }

        [Fact]
        public async Task TestResultSuccess()
        {
            var getResult = Task.FromResult(new ApiResult<Input>(HttpStatusCode.OK, new Input()));
            var request = new ApiRequest<Input>(options => getResult);

            Assert.NotNull(await request.TryAsync());
        }

        [Fact]
        public async Task TestResultFailed()
        {
            var getResult = Task.FromResult(new ApiResult<Input>(HttpStatusCode.BadRequest, new ErrorDto()));
            var request = new ApiRequest<Input>(options => getResult);

            Assert.Null(await request.TryAsync());
            Assert.Null((await request.ResultAsync()).Data);
            Assert.NotNull((await request.ResultAsync()).Error);

            var ex = await Assert.ThrowsAsync<ApiHttpException>(async () => await request.ExecAsync());

            Assert.NotNull(ex);
            Assert.NotNull(ex.Error);
        }

        [Fact]
        public void TestBearerTokenFailed()
        {
            var tokenResult = new ApiResult<TokenResponseDto>(HttpStatusCode.BadRequest, new ErrorDto() { ErrorCode = "BadRequestTest" });
            var result = tokenResult.ToBearerTokenResult();

            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            Assert.Equal("BadRequestTest", result.Error.ErrorCode);
        }
    }
}