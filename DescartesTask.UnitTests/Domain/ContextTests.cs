﻿using DescartesTask.Domain;
using NSubstitute;
using Xunit;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DescartesTask.UnitTests.Domain
{
    public class ContextTests
    {
        [Fact]
        public async Task TestMigrationShouldRun()
        {
            var database = Substitute.For<DatabaseFacade>(GetInMemoryContext());
            database.EnsureCreated().Returns(false);

            //await Assert.ThrowsAsync<InvalidOperationException>(async () => {
            //    await DbInitializer.InitializeAsync(database, true);
            //});
        }

        private RepositoryContext GetInMemoryContext(string dbName = null)
            => new RepositoryContext(GetInMemoryContextOptions(dbName));

        //Once again, UseInMemoryDatabase does not seem to work
        private DbContextOptions<RepositoryContext> GetInMemoryContextOptions(string dbName = null) =>
            new DbContextOptionsBuilder<RepositoryContext>().Options;
    }
}
