﻿using DescartesTask.Sdk.Authentication;
using DescartesTask.UnitTests.Utils;
using DescartesTask.Sdk.Util;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System;
using Xunit;

namespace DescartesTask.UnitTests.Authentication
{
    public class ClientCredentialsTokenHandlerTests
    {
        public const string ExpectedToken = "UnitTestInternalToken";
        public const string ExpectedToken2 = "UnitTestInternalToken2";

        [Fact]
        public async Task TestGetInternalToken()
        {
            var httpMessageHandler = new FakeResponseHttpMessageHandler(HttpStatusCode.OK, $"{{ access_token: '{ExpectedToken}', expires_in: 1000 }}");

            var httpClient = new HttpClient(httpMessageHandler)
            {
                BaseAddress = new Uri("https://localhost/")
            };

            var tokenHandler = new ClientCredentialsTokenHandler(httpClient, new ClientCredentialsOptions());
            var token = (await tokenHandler.GetTokenAsync()).GetData();
            Assert.Equal(ExpectedToken, token.AccessToken);

            //Check that caching works
            httpMessageHandler.StatusCode = HttpStatusCode.OK;
            httpMessageHandler.Content = $"{{ access_token: '{ExpectedToken2}', expires_in: 500 }}";

            //Fetch new token (should be the same one)
            token = (await tokenHandler.GetTokenAsync()).GetData();
            var result = (tokenHandler.GetCachedToken(DateTimeOffset.UtcNow));
            Assert.NotNull(result);
            Assert.Equal(ExpectedToken, token.AccessToken);

            //Fetch token in future 
            result = (tokenHandler.GetCachedToken(DateTimeOffset.UtcNow.AddYears(2)));
            Assert.Null(result);
        }

        [Fact]
        public async Task TestUnableToFormatToken()
        {
            var httpMessageHandler = new FakeResponseHttpMessageHandler(HttpStatusCode.OK, $"ReturnDataThatCanNotBeSerialized");

            var httpClient = new HttpClient(httpMessageHandler)
            {
                BaseAddress = new Uri("https://localhost/")
            };

            var tokenHandler = new ClientCredentialsTokenHandler(httpClient, new ClientCredentialsOptions());
            var result = await tokenHandler.GetTokenAsync();

            Assert.Equal(httpMessageHandler.StatusCode, result.StatusCode);
            Assert.Null(result.Data);
            Assert.Equal(ErrorCodes.AuthorityUnableToFormatToken, result.Error.ErrorCode);
        }

        [Fact]
        public async Task TestUnableToGetToken()
        {
            var httpMessageHandler = new FakeResponseHttpMessageHandler(HttpStatusCode.BadRequest, $"{{ error: 'Missing clientId' }}");

            var httpClient = new HttpClient(httpMessageHandler)
            {
                BaseAddress = new Uri("https://localhost/")
            };

            var tokenHandler = new ClientCredentialsTokenHandler(httpClient, new ClientCredentialsOptions());
            var result = await tokenHandler.GetTokenAsync();

            Assert.Equal(httpMessageHandler.StatusCode, result.StatusCode);
            Assert.Null(result.Data);
            Assert.Equal(ErrorCodes.AuthorityUnableToGetToken, result.Error.ErrorCode);
            Assert.Contains("Missing clientId", result.Error.Message);
        }

        [Fact]
        public async Task TestExceptionWhenFetchingToken()
        {
            var httpMessageHandler = new ExceptionHttpMessageHandler();
            var httpClient = new HttpClient(httpMessageHandler)
            {
                BaseAddress = new Uri("https://localhost/")
            };

            var tokenHandler = new ClientCredentialsTokenHandler(httpClient, new ClientCredentialsOptions());
            var result = await tokenHandler.GetTokenAsync();

            Assert.Equal(HttpStatusCode.InternalServerError, result.StatusCode);
            Assert.Null(result.Data);
            Assert.Equal(ErrorCodes.AuthorityEndpointFailiure, result.Error.ErrorCode);
        }

        [Fact]
        public async Task TestAuthorityServiceIsDown()
        {
            var httpMessageHandler = new FakeResponseHttpMessageHandler(HttpStatusCode.ServiceUnavailable, $"<html><body>Service is down</body></html>");

            var httpClient = new HttpClient(httpMessageHandler)
            {
                BaseAddress = new Uri("https://localhost/")
            };

            var tokenHandler = new ClientCredentialsTokenHandler(httpClient, new ClientCredentialsOptions());
            var result = await tokenHandler.GetTokenAsync();

            Assert.Equal(httpMessageHandler.StatusCode, result.StatusCode);
            Assert.Null(result.Data);
            Assert.Equal(ErrorCodes.AuthorityServiceUnavailable, result.Error.ErrorCode);
        }
    }
}
