﻿using DescartesTask.Sdk.Decompression;
using System.Threading.Tasks;
using System.IO.Compression;
using System.Net.Http;
using System.Text;
using System.IO;
using System;
using Xunit;

namespace DescartesTask.UnitTests.Decompression
{
    public class DecompressionTests
    {
        [Fact]
        public void TestIncorrectArguments()
        {
            ArgumentNullException ex = Assert.Throws<ArgumentNullException>(() =>
                 new CompressedContent(new StringContent("Test"), null)
            );
            Assert.NotNull(ex);


            InvalidOperationException ex2 = Assert.Throws<InvalidOperationException>(() =>
                 new CompressedContent(new StringContent("Test"), "invalid_content_type")
            );
            Assert.NotNull(ex2);
        }

        [Fact]
        public async Task TestDeflate()
        {
            string orginalString = "Test";

            //Compress
            var content = new CompressedContent(new StringContent(orginalString), "deflate");
            var compressedString = await content.ReadAsStringAsync();

            //Decompress
            string result;
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(compressedString)))
            {
                var deflateStream = new DeflateStream(stream, CompressionMode.Decompress, true);
                using StreamReader reader = new StreamReader(deflateStream, Encoding.UTF8);
                result = reader.ReadToEnd();
            }

            //Compare
            Assert.Equal(orginalString, result);
        }
    }
}
