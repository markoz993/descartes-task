﻿using DescartesTask.Service.AutoMapper;
using DescartesTask.Domain.Entities;
using DescartesTask.Domain.DTOs;
using AutoMapper;
using Xunit;

namespace DescartesTask.UnitTests.Service.Automapper
{
    public class AutoMapperTests
    {
        readonly IMapper mapper = AutoMapperConfiguration.Configure();

        [Fact]
        public void MappingIsCorrect()
        {
            mapper.ConfigurationProvider.AssertConfigurationIsValid();
        }

        [Fact]
        public void TestMapNull()
        {
            Assert.Null(((Input)null).MapTo<InputDto>(mapper));
        }

        [Fact]
        public void TestMapEvent()
        {
            Assert.NotNull(new Input().MapTo<InputDto>(mapper));
        }
    }
}